#!/usr/bin/env python3
# Bryant Hansen

# USAGE:
#    sys.argv[0] media_file 
# where media_file full path/filename to mplayer-compatible file on the local
# system
#
# initial conditions: pyplayerd is running

import os
import sys
from time import sleep
import traceback


import pyplayer_config as config
import lib.pyplayerc as pyplayerc
from multiprocessing.connection import Client

#####################################################
# Main

if __name__ == "__main__":
    USAGE = sys.argv[0] + "  command args"
    host = 'localhost'
    port = 17000

    if not os.path.exists(config.pidfile):
        raise RuntimeError('pyplayerd pidfile %s not found; it appears to not be running.  (tip: execute "path-to/pyplayerd start")' % config.pidfile)

    # TODO: check for python process running

    try:
        c = Client(
                (host, port),
                authkey=b'super_secret_auth_key')
    except ConnectionRefusedError:
        print("Connection Refused on %s:%d.  Is the daemon running?"
              % (host, port))
        sys.exit(2)
    except:
        print("*** print_exc:")
        traceback.print_exc()
        sys.exit(3)
    proxy = pyplayerc.RPCProxy(c)


    result = "not set"
    try:
        if len(sys.argv) >= 3:
            args = sys.argv[2:]
            # $ ./016_do_command_with_args.py get_title a b
            # result = getattr(proxy, 'get_title')(a,b)
            # proxy.dump_queue result: not set
            sys.stdout.write("result = getattr(proxy, '%s')(%s)\n" % (sys.argv[1], ",".join(args)))
            #result = getattr(proxy, )(a,b)
            result = getattr(proxy, sys.argv[1])('\n'.join(sys.argv[2:]))
        else:
            #result = getattr(proxy, sys.argv[1])()
            sys.stdout.write("result = getattr(proxy, '%s')()\n" % (sys.argv[1]))
            result = getattr(proxy, sys.argv[1])()
    except:
        print("*** print_exc:")
        traceback.print_exc()
        sys.exit(4)
        
    print("proxy.%s: %s\n" % (sys.argv[1], str(result)))
