#!/bin/sh
# Bryant Hansen

# initial 1-liner:
# pyplayerd.py stop ; pyplayerd.py start ; pyplayer add_url_to_queue /data/media/mp3/Rammstein/Liebe_Ist_Für_Alle_Da/04\ -\ Haifisch.mp3 ; pyplayer play_next_url ; sleep 5 ; pyplayer get_title

file="/data/media/mp3/Rammstein/Liebe_Ist_Für_Alle_Da/04\ -\ Haifisch.mp3"
[[ "$1" ]] && file="$1"

pyplayerd.py stop
pyplayerd.py start
pyplayer add_url_to_queue "$file"
pyplayer play_next_url
sleep 5
title="$(pyplayer get_title)"

FAIL=""
if [[ "$title" == "Haifisch" ]] ; then
    echo "TEST PASSED"
else
    echo "TEST FAILED"
    FAIL=1
fi

track="$(pyplayer get_track)"

percent_pos1="$(pyplayer get_percent_pos)"
sleep 5
percent_pos1="$(pyplayer get_percent_pos)"
get_album="$(pyplayer get_album)"
echo "get_album: $get_album"

get_all_information="$(pyplayer get_all_information)"
echo "get_all_information: $get_all_information"

get_artist="$(pyplayer get_artist)"
echo "get_artist: $get_artist"

get_audio_bitrate="$(pyplayer get_audio_bitrate)"
echo "get_audio_bitrate: $get_audio_bitrate"

get_audio_samples="$(pyplayer get_audio_samples)"
echo "get_audio_samples: $get_audio_samples"

get_current_time="$(pyplayer get_current_time)"
echo "get_current_time: $get_current_time"

get_file_name="$(pyplayer get_file_name)"
echo "get_file_name: $get_file_name"

get_meta_album="$(pyplayer get_meta_album)"
echo "get_meta_album: $get_meta_album"

get_meta_artist="$(pyplayer get_meta_artist)"
echo "get_meta_artist: $get_meta_artist"

get_meta_comment="$(pyplayer get_meta_comment)"
echo "get_meta_comment: $get_meta_comment"

get_meta_genre="$(pyplayer get_meta_genre)"
echo "get_meta_genre: $get_meta_genre"

get_meta_title="$(pyplayer get_meta_title)"
echo "get_meta_title: $get_meta_title"

get_meta_track="$(pyplayer get_meta_track)"
echo "get_meta_track: $get_meta_track"

get_meta_year="$(pyplayer get_meta_year)"
echo "get_meta_year: $get_meta_year"

get_percent_pos="$(pyplayer get_percent_pos)"
echo "get_percent_pos: $get_percent_pos"

get_state="$(pyplayer get_state)"
echo "get_state: $get_state"

get_sub_visibility="$(pyplayer get_sub_visibility)"
echo "get_sub_visibility: $get_sub_visibility"

get_time_length="$(pyplayer get_time_length)"
echo "get_time_length: $get_time_length"

get_time_pos="$(pyplayer get_time_pos)"
echo "get_time_pos: $get_time_pos"

get_title="$(pyplayer get_title)"
echo "get_title: $get_title"

get_track="$(pyplayer get_track)"
echo "get_track: $get_track"

get_track_length="$(pyplayer get_track_length)"
echo "get_track_length: $get_track_length"

get_url="$(pyplayer get_url)"
echo "get_url: $get_url"

get_video_bitrate="$(pyplayer get_video_bitrate)"
echo "get_video_bitrate: $get_video_bitrate"

get_video_codec="$(pyplayer get_video_codec)"
echo "get_video_codec: $get_video_codec"

get_video_resolution="$(pyplayer get_video_resolution)"
echo "get_video_resolution: $get_video_resolution"

get_vo_fullscreen="$(pyplayer get_vo_fullscreen)"
echo "get_vo_fullscreen: $get_vo_fullscreen"

get_volume1="$(pyplayer get_volume1)"
echo "get_volume1: $get_volume1"

get_volume2="$(pyplayer get_volume2)"
echo "get_volume2: $get_volume2"

