#!/usr/bin/env python3
# Bryant Hansen

# USAGE:
#    sys.argv[0] media_file 
# where media_file full path/filename to mplayer-compatible file on the local
# system
#
# initial conditions: pyplayerd is running

import os
import sys
from time import sleep
import traceback


import pyplayer_config as config
import lib.pyplayerc as pyplayerc
from multiprocessing.connection import Client

#####################################################
# Main

if __name__ == "__main__":
    USAGE = sys.argv[0] + "  media_file"
    host = 'localhost'
    port = 17000

    if not os.path.exists(config.pidfile):
        raise RuntimeError('pyplayerd pidfile %s not found; it appears to not be running.  (tip: execute "path-to/pyplayerd start")' % config.pidfile)

    # TODO: check for python process running

    try:
        c = Client(
                (host, port),
                authkey=b'super_secret_auth_key')
    except ConnectionRefusedError:
        print("Connection Refused on %s:%d.  Is the daemon running?"
              % (host, port))
        sys.exit(2)
    except:
        print("*** print_exc:")
        traceback.print_exc()
        sys.exit(3)
    proxy = pyplayerc.RPCProxy(c)
    result = proxy.get_title()
    print("proxy.get_title() = %s" % str(result))
    s = proxy.get_percent_pos()
    print('proxy.get_percent_pos() = %s' % s)
    s = proxy.get_track_length()
    print('proxy.get_track_length() = %s' % s)

"""
#s = perform_command(, cmd, expect="")
#TRACE('perform_command(, cmd, expect="") = %s' % s)
s = get_current_time()
print('get_current_time() = %s' % s)
s = get_state()
TRACE('get_state() = %s' % s)
s = get_title()
TRACE('get_title() = %s' % s)
s = get_artist()
TRACE('get_artist() = %s' % s)
s = get_album()
TRACE('get_album() = %s' % s)
s = get_track()
TRACE('get_track() = %s' % s)
s = get_genre()
TRACE('get_genre() = %s' % s)
s = get_year()
TRACE('get_year() = %s' % s)
s = get_volume1()
TRACE('get_volume1() = %s' % s)
s = get_volume2()
TRACE('get_volume2() = %s' % s)
s = set_current_pos(, position)
TRACE('set_current_pos(, position) = %s' % s)
s = seek(, reltime)
TRACE('seek(, reltime) = %s' % s)
s = pause()
TRACE('pause() = %s' % s)
s = get_all_information()
TRACE('get_all_information() = %s' % s)
s = mplayer_thread(, threadName, mplayer_start_event, url)
TRACE('mplayer_thread(, threadName, mplayer_start_event, url) = %s' % s)
"""
