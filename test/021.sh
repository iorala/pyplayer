#!/bin/sh
# Bryant Hansen

# initial 1-liner:
# pyplayerd.py stop ; pyplayerd.py start ; pyplayer add_url_to_queue /data/media/mp3/Rammstein/Liebe_Ist_Für_Alle_Da/04\ -\ Haifisch.mp3 ; pyplayer play_next_url ; sleep 5 ; pyplayer get_title

pyplayerd.py stop
pyplayerd.py start
pyplayer add_url_to_queue /data/media/mp3/Rammstein/Liebe_Ist_Für_Alle_Da/04\ -\ Haifisch.mp3
pyplayer play_next_url
sleep 5
title="$(pyplayer get_title)"

pyplayer get_url

if [[ "$title" == "Haifisch" ]] ; then
    echo "TEST PASSED"
    exit 0
else
    echo "TEST FAILED"
    exit 1
fi

