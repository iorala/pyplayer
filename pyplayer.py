#!/usr/bin/env python3
"""
@author: Bryant Hansen
@license: GPLv3

# USAGE:
#    sys.argv[0] command [ arg1 ... ]
#
# initial conditions: pyplayerd is running

@brief A dual interface as both a wrapper for the pyplayerd, but also a client
implementation importing pyplayerc
"""

import sys
import os

from time import sleep
from datetime import datetime
import traceback
import pyplayerd

from multiprocessing.connection import Client

import config
import lib.pyplayerc as pyplayerc

from socket import error as socket_error

import pwd

ME = os.path.basename(sys.argv[0])

import logging
logger = None

#####################################################
# Functions

def TRACE(msg, level = logging.INFO):
    #m = ("%s: %s.pyplayer(%d): %s"
    #   % (datetime.now().strftime("%H:%M:%S.%f")[0:-3], ME, os.getpid(), msg))
    #if logger.handlers:
    if logger == None:
        #logging.log(level, m)
        sys.stderr.write("pyplayer stderr: %s: %s\n" % (str(level), msg))
    else:
        logger.log(level, msg)

def get_username():
    return pwd.getpwuid( os.getuid() )[ 0 ]

#####################################################
# Main

if __name__ == "__main__":
    USAGE = sys.argv[0] + "  command args"

    logger = logging.getLogger()
    #logger.setLevel(logging.DEBUG)
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    #ch.setLevel(logging.DEBUG)

    # create formatter
    #formatter = logging.Formatter(
    #            "%(asctime)s: pyplayer: %(name)s: %(levelname)s: %(message)s")

    if not logger.handlers:
        # add ch to logger
        logger.addHandler(ch)

    '''
    # calling the logger at different levels
    logger.debug("pyplayer.py debug message")
    logger.info("pyplayer.py info message")
    logger.warn("pyplayer.py warn message")
    logger.error("pyplayer.py error message")
    logger.critical("pyplayer.py critical message")
    '''

    '''
    FORMAT = '%(asctime)-15s %(user)-8s %(message)s'
    logging.basicConfig(format=FORMAT)
    '''

    if not os.path.exists(config.pidfile):
        logger.error(
                'pyplayerd pidfile %s not found; it appears to not be running.'
                '  (tip: execute "path-to/pyplayerd start")\n' % config.pidfile)
        sys.exit(1)

    # TODO: check for python process running

    try:

        logger.info("connecting to server '%s' on port %s"
              % (config.SERVER, config.PORT))
        c = Client(
            (config.SERVER, config.PORT),   authkey=config.authkey)
    except socket_error as serr:
        if serr.errno != errno.ECONNREFUSED:
            # Not the error we are looking for, re-raise
            print("Connection Refused on %s:%d.  Is the daemon running?"
                % (config.SERVER, config.PORT))
            sys.exit(2)
    except:
        e = sys.exc_info()
        logger.error("exception connecting to server; exception info: %s" % str(e))
        logger.error("*** print_exc:")
        traceback.print_exc()
        sys.exit(3)
    proxy = pyplayerc.RPCProxy(c)

    response = "no response"
    retval = 0
    try:
        if len(sys.argv) <= 1 or sys.argv[1] == "help":
            sys.stdout.write(''.join([
                "\n#############################################\n"
                "# ", ME, " help:\n"
                "\n"
                "This command opens a connection to the pyplayerd daemon and "
                "instructs it to execute a command/request.\n"
                "The command is the first argument of this.\n"
                "\n"
                "   ", ME, " command [ args ... ] \n"
                "\n"
                "Examples: \n"
                "   ", ME, " add_url_to_queue /data/media/mp3/cool_tune.mp3 \n"
                "   ", ME, " dump_queue \n"
                "   ", ME, " play_next_url \n"
                "\n"
                "The full list of commands that ", ME, " supports is shown "
                "below in the pyplayerd help.\n"
                "\n"
                "Note: tab completion should be supported by performing the "
                "following:\n"
                "\n"
                "   . pyplayer_bashcomp.sh\n"
                "\n"
                "(your humble author finds this a bit more convenient than "
                "having to remember all of the subcommands)\n"
                "\n"
                "====================================\n"
                "pyplayerd help:\n"
                "====================================\n"
            ]))
            pyplayerd.help()
            sys.stdout.write(
                "\n\n"
                "====================================\n"
                "Installation\n"
                "====================================\n"
                " - ensure that both pyplayer.py and pyplayerd.py "
                "are in your path or call them with full pathname\n"
            )
            response = ("pyplayerd.help() called to return a portion of the "
                        "help response")
            user = ""
            try:
                user = " (%s)" % get_username()
            except:
                pass
            sys.stdout.write(''.join([
                " - verify that the following directories exists "
                "and are writable by the current user ", user, ":\n"
                "   - /var/log/pyplayer/\n"
                "   - /run/pyplayer/\n"
                "\n"
                "====================================\n"
                "Troubleshooting\n"
                "====================================\n\n"
            ]))
        elif len(sys.argv) >= 3:
            # TODO: send the current working directory as an argument
            # We're not parsing which args are files, possibly with relative
            # paths.
            # The server must have this information to find the files in the
            # filesystem that are references by paths relative to the current
            # dir.
            args = [ os.getcwd(), ', '.join(sys.argv[2:]) ]
            logger.info("args: %s" % str(args))
            try:
                response = getattr(proxy, sys.argv[1])('\n'.join(args))
                logger.info("getattr(proxy, '%s')(%s) response: '%s'"
                      % (sys.argv[1], ",".join(args), str(response)))
            except:
                logger.error("exception %s in getattr(proxy, %s)(args = %s); "
                      "it appears that this attribute does not exist"
                      % (
                         str(sys.exc_info()),
                         str(sys.argv[1]),
                         str(sys.argv[2:])
                        )
                     )
        #elif len(sys.argv) >= 2:
        else:
            logger.debug("getattr(proxy, '%s')()" % (sys.argv[1]))
            try:
                response = getattr(proxy, sys.argv[1])()
            except:
                logger.error("exception %s in getattr(proxy, %s); "
                      "it appears that this attribute does not exist"
                      % (str(sys.exc_info()), str(sys.argv[1])))
                response = "<no function matching '%s'>" % sys.argv[1]
                retval = 3
    except:
        e = sys.exc_info()
        logger.error("exception connecting to server; exception info: %s" % str(e))
        response = str(e)
        logger.error("*** print_exc:")
        traceback.print_exc()
        response = "<exception handling request '%s'>" % str(sys.argv)
        retval = 4

    str_result = str(response)
    if len(sys.argv) >= 2:
        logger.info("proxy.%s response: %s\n" % (sys.argv[1], str_result))
    str_result = str_result.replace("'", "")
    sys.stdout.write("%s\n" % (str_result))
    logger.debug("%s exiting normally." % (sys.argv[0]))
    sys.exit(retval)

