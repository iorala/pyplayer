#!/usr/bin/python3 -u
# @author: Bryant Hansen
# @license: GPLv3

"""
Note: the append and pop functions in deque are thread-safe
"""

# TODO: remove the unnecessary ones

import sys
#import stat
import os
import time
#import signal
#import subprocess
import logging
from time import sleep
from datetime import datetime, timedelta
from threading import Thread, Event
from collections import deque
import config
import traceback

#####################################################
# Functions

ME = sys.argv[0]
def TRACE(msg, level = logging.INFO):
    m = ("%s: %s.pyplayer_queue(%d): %s"
         % (datetime.now().strftime("%H:%M:%S.%f")[0:-3], ME, os.getpid(), msg))
    #logging.log(level, m)
    sys.stderr.write("%s: %s\n" % (str(level), m))

class Play_Queue(object):

    
    __queue = deque()

    def dump_queue(self):
        if len(self.__queue) <= 0:
            TRACE("dump_queue: the queue is empty")
            return "<queue-empty>"
        TRACE("__queue contents:")
        str_list = ""
        n = 0
        for item in self.__queue:
            n = n + 1
            TRACE("  %d. %s" % (n, str(item)))
            str_list += "%s\n" % str(item)
        return str_list

    def add_url_to_queue(self, url):
        try:
            # What type/format would multiple URLs be in
            # for now, let's call it a string which may contain carraige returns, due to ease of implementation 
            if isinstance(url, str):
                TRACE("url(%s) is a string; split it by carriage returns" % url)
                urls = url.split('\n')
                TRACE("urls: %s" % str(urls))
            else:
                urls = [ url ]
            for u in urls:
                self.__queue.append(u)
                TRACE(
                    "%s: %d: %s"
                     % (sys._getframe().f_code.co_name, len(self.__queue), str(u)))
            return "added to queue"
        except:
            TRACE(
                "Exception in add_url_to_queue: %s"
                % str(sys.exc_info()))
            traceback.print_exc()
            return "exception adding element to the queue"

    def queue_url_next(self):
        TRACE(
            "%s: queuing URL '%s' next.  %d total items"
            % (sys._getframe().f_code.co_name, url, len(self.__queue)))
        self.__queue.appendleft(url)
        return "url queued next"

    def queue_remove_item(self, itemname):
        TRACE("%s: queuing URL '%s' next.  %d total items"
              % (sys._getframe().f_code.co_name, url, len(self.__queue)))
        self.__queue.appendleft(url)
        return "url queued next"

    def _get_next_in_queue(self):
        try:
            if len(self.__queue) < 1:
                TRACE("%s: the queue is empty" % sys._getframe().f_code.co_name)
                return None
            url = self.__queue.popleft()
            TRACE(
                "%s: retrieved URL '%s' from the queue.  %d items left"
                % (sys._getframe().f_code.co_name, url, len(self.__queue)))
            self.dump_queue()
            return url
        except:
            TRACE(
                "Exception in _get_next_in_queue: %s"
                % str(sys.exc_info()))
            return None

    def flush_queue(self):
        TRACE("%s: flushing the queue" % sys._getframe().f_code.co_name)
        self.__queue.clear()
        return "queue flushed"

    def replace_queue_with_url(self, url):
        TRACE(
            "%s: replacing the entire queue with the single URL '%s'"
            % (sys._getframe().f_code.co_name, url))
        self.__queue.clear()
        self.__queue.append(url)
        return "queue replaced with url"

