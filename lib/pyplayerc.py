#!/usr/bin/env python3
# daemon.py

"""
from the Python Cookbook (version 3)
  11.8. Implementing Remote Procedure Calls

TODO: determine overhead; can a new instance be loaded for each web request?
...or must it be static?

usage example:

import pyplayerc
from multiprocessing.connection import Client
c = Client(('localhost', 17000), authkey=b'super_secret_auth_key')
proxy = pyplayerc.RPCProxy(c)
#proxy.add(2, 3) # this sample function is now obsolete.  TODO: find another, like status or something
proxy.dump_queue()

"""

import json
from multiprocessing.connection import Client

class RPCProxy:
    def __init__(self, connection):
        self._connection = connection
    def __getattr__(self, name):
        def do_rpc(*args, **kwargs):
            self._connection.send(json.dumps((name, args, kwargs)))
            try:
              result = json.loads(self._connection.recv())
            except:
              import sys
              sys.stderr.write(
                  "Exception in __subprocess_output_readline: %s"
                  % str(sys.exc_info())
              )
              result = None
              #stderr.write("pyplayerc exception: %s" % str(result))
            return result
        return do_rpc

