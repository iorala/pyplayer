#!/bin/sh
# Bryant Hansen

ME="$(basename "$0")"

PYPLAYERD=/usr/local/bin/pyplayerd.py
RUN_FILE=/run/pyplayer/pyplayerd.pid
LOG_FILE=/var/log/pyplayer/pyplayerd.log
PYPLAYERD_USER=pyplayerd
PYPLAYERD_GROUP=pyplayerd
PROJECT_DIR=/opt/critter_list

CONF_FILE="/etc/pyplayer/pyplayerd.conf"
[[ -f "$CONF_FILE" ]] && . "$CONF_FILE"
CONF_FILE="${HOME}/pyplayer/pyplayerd.conf"
[[ -f "$CONF_FILE" ]] && . "$CONF_FILE"
CONF_FILE="./pyplayerd.conf"
[[ -f "$CONF_FILE" ]] && . "$CONF_FILE"
CONF_FILE="/etc/pyplayer/pyplayerd.conf"

config="
            RUN_FILE=$RUN_FILE
            LOG_FILE=$LOG_FILE
            PYPLAYERD_USER=$PYPLAYERD_USER
            PYPLAYERD_GROUP=$PYPLAYERD_GROUP
            PYPLAYERD=$PYPLAYERD
        "
if [[ ! "$GIT_URL" ]] ; then
    echo "$ME ERROR: GIT_URL not set." >&2
    exit 2
fi
commands="
    if [ ! -d '$PROJECT_DIR' && '$GIT_URL' ] ; then
        if pushd '$(dirname "$PROJECT_DIR")' ; then
            if [ ! -d '"$(dirname "$PROJECT_DIR")"' ] ; then
                if mkdir -p '"$(dirname "$PROJECT_DIR")"' ; then
                    echo \"cloning '$GIT_URL' to '$PROJECT_DIR'...\"
                    if git clone '$GIT_URL' ; then
                        echo 'git clone successful'
                    else
                        echo 'git clone failed'
                    fi
                fi
            fi
            popd
        fi
    fi

    if [ ! -d '$PROJECT_DIR' && '$HTTPS_URL' ] ; then
        pushd '$(dirname "$PROJECT_DIR")' && ( wget -r -nc '$HTTPS_URL' ; popd )
    fi

    if [ ! -d '$(dirname "$CONF_FILE")' ] ; then
        mkdir -p '$(dirname "$CONF_FILE")'
    fi
    chown ${PYPLAYERD_USER}:${PYPLAYERD_GROUP} '$(dirname "$CONF_FILE")'
    chmod g-w '$(dirname "$CONF_FILE")'

    if [ ! -f '$CONF_FILE' ]] ; then
        echo -e '$config' | sed 's/^[ \\t]*//' > '$CONF_FILE'
    fi
    chown ${PYPLAYERD_USER}:${PYPLAYERD_GROUP} '$CONF_FILE'

    if ! id -u $PYPLAYERD_USER ; then
        useradd $PYPLAYERD_USER
    fi
    if ! id -g $PYPLAYERD_GROUP ; then
        groupadd $PYPLAYERD_GROUP
    fi

    if [ ! -d '$(dirname "$RUN_FILE")' ] ; then
        mkdir -p '$(dirname "$RUN_FILE")'
    fi
    chown ${PYPLAYERD_USER}:${PYPLAYERD_GROUP} '$(dirname "$RUN_FILE")'

    if [ ! -d '$(dirname "$LOG_FILE")' ] ; then
        mkdir -p '$(dirname "$LOG_FILE")'
    fi
    chown ${PYPLAYERD_USER}:${PYPLAYERD_GROUP} '$(dirname "$LOG_FILE")'
"

FILENAME_PREFORMATTER_REGEX="s/\#.*//;/^[ \t]*$/d;s/^[ \t]*//;s/[ \t]*$//"
unset dst
MANIFEST_FILE=install/MANIFEST.lst
echo "# MANIFEST_FILE $MANIFEST_FILE" >&2
cat $MANIFEST_FILE \
| sed "$FILENAME_PREFORMATTER_REGEX" \
| while read line ; do
  # echo "# $line" >&2
  if [[ ! "${line//\[*\]/}" ]] ; then
      dst="${line#*\[}"
      dst="${dst%\]*}"
      echo "## (dst=$dst) " >&2
  else
      if [[ ! "$dst" ]] ; then
          echo "# ERROR: dst not set!" >&2
          break
      fi
      dstdir="$(dirname "$dst")"
      if [[ ! -d "$dstdir" ]] ; then
          echo "mkdir -p '$dstdir'"
      fi
      src="${line#*\[}"
      src="${src%\]*}"
      if [[ ! -f "$src" ]] ; then
          echo "# ERROR: src '$src' does not exist!" >&2
          break
      fi
      b="$(basename "$src")"
      if [ -f "${dst}/${b}" ] ; then
          echo "# ${dst}/${b} already exists" >&2
      else
          echo "cp -a '${src}' '${dst}'"
      fi
  fi
done

echo "######################################################"
echo "# To install, simply execute the following commands as root or sudo:"
echo "######################################################"
echo -e "$commands"
echo "######################################################"
echo ""
echo "# Tip: root can pipe the commands to the shell, like so: "
echo "#   $0 | ${SHELL}"

#if [[ $UID == 0 ]] ; then
#    groupadd pyplayer
#    [[ ! -d /run/pyplayer ]] && mkdir -p /run/pyplayer
#    chgrp pyplayer /run/pyplayer
#    chmod g+wrx /run/pyplayer
#    cp -a  "$SOURCE_DIR/pyplayer/pyplayer_bashcomp.sh" "/usr/share/bashcomp/pyplayer"
#else
#    home="~"
#    [[ "$HOME" ]] &&                             home="$HOME"
#    #[[ ! -d "$home"/run/pyplayer ]] &&           mkdir -p "$home"/run/pyplayer
#    [[ ! -d "$home"/.critter_list ]] &&          mkdir -p "$home"/.critter_list
#    if [[ -f "$SOURCE_DIR"/pyplayer/pyplayer_bashcomp.sh ]] ; then
#        ln -fs  "$SOURCE_DIR"/pyplayer/pyplayer_bashcomp.sh "$home"/.bash_completion.d/pyplayer
#    fi
#    if [[ -f "/usr/share/bashcomp/pyplayer/pyplayer_bashcomp.sh" ]] ; then
#        ln -fs  "/usr/share/bashcomp/pyplayer/pyplayer_bashcomp.sh" "$home"/.bash_completion.d/pyplayer
#    fi
#fi

