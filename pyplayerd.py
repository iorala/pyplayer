#!/usr/bin/env python3
# daemon.py

"""
originally from the Python Cookbook (version 3)
  11.8. Implementing Remote Procedure Calls
  12.2. Determining If a Thread Has Started
  12.14. Launching a Daemon Process on Unix
  (perhaps more)

majorly customized by bryant hansen
"""

from lib.pyplayer_mplayer_if import MPlayerIF
import config

import os
import sys
import time
from datetime import datetime, timedelta
from threading import Thread

import atexit
import signal

from multiprocessing.connection import Listener
import json

from inspect import getmembers, isfunction, ismethod
import traceback

ME = sys.argv[0]

import logging
logger = None

# for granting permissions to other users to run
pid_dir_group = "pyplayerd"

def TRACE(msg, level = logging.INFO):
    if logger == None:
        logging.log(level, msg)
    else:
        logger.log(level, msg)

# TODO: figure out what I was trying to do here years ago
def enumerate_functions2():
    '''
    @brief this function enumerates all of the functions supported by
           the instance of the MPlayerIF: mplif
           the list carriage-return delimited
           this function returns a list of strings
    '''
    function_name = sys._getframe().f_code.co_name
    functions = ()
    try:
        handler = RPCHandler()
        first = True
        for f in getmembers(handler.mplif):
            if f[1] is None: continue
            if ismethod(f[1]) and f[0][0] != '_':
                functions.append(str(f[0]))
                TRACE("%s: %s\n" % (function_name, str(f[0])))
    except:
        TRACE('exception enumerating functions')
        e = sys.exc_info()
        TRACE("exception info: %s" % str(e))
        traceback.print_exc()

def enumerate_functions():
    '''
    @brief this function enumerates all of the functions supported by the
           instance of the MPlayerIF: mplif
           the list carriage-return delimited
           it is written to stdout
    '''
    try:
        handler = RPCHandler()
        sys.stdout.write("\nFunctions supported by %s:\n" % ME)
        first = True
        for f in getmembers(handler.mplif):
            if f[1] is None: continue
            if ismethod(f[1]) and f[0][0] != '_':
                sys.stdout.write("%s\n" % (str(f[0])))
    except:
        TRACE('exception enumerating functions')
        e = sys.exc_info()
        TRACE("exception info: %s" % str(e))
        traceback.print_exc()

def enumerate_functions_compact():
    '''
    @brief this function enumerates all of the functions supported by the
           instance of the MPlayerIF: mplif
           the list is comma-delimited with a single carriage return on the end
           it is written to stdout
    @NOTE the format of the output is not compatible with the current bashcomp
          (bash completion) parser, pyplayer_bashcomp.sh
          the output of the enumerate functions
    '''
    try:
        handler = RPCHandler()
        sys.stdout.write("\nFunctions supported by %s:\n" % ME)
        first = True
        for f in getmembers(handler.mplif):
            if f[1] is None: continue
            if ismethod(f[1]) and f[0][0] != '_':
                if not first: sys.stdout.write(", ")
                first = False
                sys.stdout.write("%s" % (str(f[0])))
        sys.stdout.write("\n")
    except:
        TRACE('exception enumerating functions')
        e = sys.exc_info()
        TRACE("exception info: %s" % str(e))
        traceback.print_exc()

class RPCHandler:

    #logging.basicConfig(format='%(asctime) %(message)s')

    def __init__(self):
        self._functions = { }
        self.mplif = MPlayerIF()

    def register_function(self, func):
        TRACE("adding function %s" % (str(func.__name__)))
        self._functions[func.__name__] = func

    def handle_connection(self, connection):
        func_name = ""
        try:
            while True:
                # Receive a message
                func_name, args, kwargs = json.loads(connection.recv())
                TRACE(
                    "RPCHandler.handle_connection: (%s) called (args = %s)"
                    % (func_name, str(args))
                )
                sys.stdout.flush()
                # Run the RPC and send a response
                try:
                    r = self._functions[func_name](*args,**kwargs)
                    sres = json.dumps(r)
                    if isinstance(r, str):
                        TRACE(
                            "RPCHandler.handle_connection: "
                            "json response to %s:\n%s"
                            % (func_name, str(r))
                        )
                    else:
                        TRACE(
                            "RPCHandler.handle_connection: "
                            "function %s: json response = '%s', "
                            "processed = %s, type = %s"
                            % (func_name, str(r), str(sres), str(type(r)))
                        )
                    connection.send(sres)
                except Exception as e:
                    TRACE(
                        "RPCHandler.handle_connection(%s): exception (%s)"
                        % (str(func_name), str(e))
                    )
                    #connection.send("exception %s" % json.dumps(str(e)))
                    connection.send("exception %s" % str(e))
                    traceback.print_exc()
                if func_name == "help":
                    sys.stdout.write("%s(%d) functions:\n"
                                     % (sys.argv[0], os.getpid()))
                    enumerate_functions()
                    #sys.stdout.write("\n")
                    #sys.stdout.write("%s: /usr/local/bin/pyplayerd.py(13127): PIDFILE = '%s'\n" % (timestamp(), PIDFILE))
                    #sys.stdout.write("/usr/local/bin/pyplayerd.py is running (pid=13122)\n")

        except EOFError:
            pass

def check_pid(pid):
    """ Check For the existence of a unix pid. """
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True

def makedirs(name, mode=0o777):
    # stolen and modified from os
    """makedirs(path [, mode=0777])

    Super-mkdir; create a leaf directory and all intermediate ones.
    Works like mkdir, except that any intermediate path segment (not
    just the rightmost) will be created if it does not exist.  This is
    recursive.

    """
    head, tail = os.path.split(name)
    while not tail:
        head, tail = os.path.split(head)
    if head and tail and not os.path.exists(head):
        makedirs(head, mode)
    try:
        os.mkdir(name, mode)
    except OSError:
        pass

def daemonize(pidfile, *,
              stdin='/dev/null',
              stdout='/dev/null',
              stderr='/dev/null'):

    # TODO: verify the directory that contains the PID file, both the existence
    #       and the writability for the current user

    d = os.path.dirname(pidfile)
    if not os.path.exists(d):
        os.makedirs(d, 0o771)
    if not os.path.exists(d):
        sys.stdout.write(
          "ERROR: run dir '%s' does not exist and we failed to create it\n"
          % d)
    if not os.path.exists(d):
        sys.stdout.write('Not running.  '
              'pyplayer run directory %s does not exist.  this '
              'folder must be created, with write and excute permissions of '
              'the current process\n' % d)
        raise SystemExit(1)

    if os.path.exists(pidfile):
        with open(pidfile) as f:

            pid = -1
            try:
                pid = int(f.read())
            except:
                TRACE('exception reading the process id file %s' % pidfile)
                e = sys.exc_info()
                TRACE("exception info: %s" % str(e))
                traceback.print_exc()
                raise RuntimeError('pidfile %s exists, '
                                   'but an exception was generated reading it'
                                   % (pidfile))
            if not check_pid(pid):
                raise RuntimeError(
                        'pidfile %s exists, but process %d is not running'
                        % (pidfile, pid))

            # verify process name
            cmdlinefile = "/proc/%d/cmdline" % pid
            if not os.path.exists(cmdlinefile):
                raise RuntimeError(
                   'Not running.  '
                   'cmdline file %s does not exist in /proc tree.\n'
                   % cmdlinefile
                )

            try:
                s = open(cmdlinefile).readline()
                # example: s='python3\x00./pyplayerd.py\x00start\x00'
                s2 = s.split("\0")[1]
                # example: s2='./pyplayerd.py'
            except:
                #sys.stdout.write('DAEMON %s is already running\n'
                #                  % str(DAEMON))
                #e = sys.exc_info()
                #TRACE("exception info: %s" % str(e))
                #traceback.print_exc()
                raise RuntimeError(
                    'Exception reading/parsing the /proc/<pid>/cmdline file %s\n'
                    % str(cmdlinefile))

            if s2.endswith(DAEMON):
                #raise RuntimeError('Already running')
                #sys.stdout.write('DAEMON %s is already running\n'
                                  #% str(DAEMON))
                raise RuntimeError('DAEMON %s is already running\n'
                                   % str(DAEMON))
                #return
            else:
                raise RuntimeError(
                    'Not running.  '
                    'process cmdline file %s indicates %s; expected %s\n'
                    % (cmdlinefile, s2, DAEMON)
                )
    # make sure the directory is writable to the user contexts that this is
    # expected to be called from
    '''
    # this is done later - TODO: remove
    from os import chmod, chown
    try:
      chmod(d, 0o775)
      chmod(pidfile, 0o664)
      TRACE('set permissions of "%s" to 0o770\n' % d)

    except:
      sys.stdout.write('Failed to set permissions of "%s" to 0o770\n' % d)

    gid=os.getgid()
    try:
      from pwd import getpwnam
      gid=getpwnam(pid_dir_group).pw_gid
      chown(d, os.getuid(), gid)
      chown(pidfile, os.getuid(), gid)
      TRACE('set group permissions of %s to %s\n' % (d, pid_dir_group))
    except:
      TRACE('Failed to set group ownership of "%s" to "%s"\n'
                       % (d, pid_dir_group))

    else:
        TRACE(
            "pidfile %s does not exist.  "
            "assume the process is not running and fork."
            % pidfile)
    '''

    # First fork (detaches from parent)
    try:
        if os.fork() > 0:
            raise SystemExit(0) # Parent exit
    except OSError as e:
        raise RuntimeError('fork #1 failed.')

    os.chdir('/')
    os.umask(0)
    os.setsid()
    # Second fork (relinquish session leadership)
    try:
        if os.fork() > 0:
            raise SystemExit(0)
    except OSError as e:
        raise RuntimeError('fork #2 failed.')

    TRACE('process forked')

    # Flush I/O buffers
    sys.stdout.flush()
    sys.stderr.flush()

    # Replace file descriptors for stdin, stdout, and stderr
    with open(stdin, 'rb', 0) as f:
        os.dup2(f.fileno(), sys.stdin.fileno())
    with open(stdout, 'ab', 0) as f:
        os.dup2(f.fileno(), sys.stdout.fileno())
    TRACE("open ab1")
    #with open(stderr, 'ab', 0) as f:
    #    os.dup2(f.fileno(), sys.stderr.fileno())
    #TRACE("open ab2")
    # Write the PID file
    d = os.path.dirname(pidfile)
    if not os.path.exists(d):
        os.makedirs(d, 0o770)
    if not os.path.exists(d):
        sys.stdout.write(
          "ERROR: run dir '%s' does not exist and we failed to create it\n"
          % d)
    TRACE("# Write the PID file %s" % pidfile)
    with open(pidfile,'w') as f:
        print(os.getpid(),file=f)

    # Arrange to have the PID file removed on exit/signal
    atexit.register(lambda: os.remove(pidfile))
    # Signal handler for termination (required)

    def sigterm_handler(signo, frame):
        TRACE("sigterm_handler(signo=%s, frame=%s)" % (str(signo), str(frame)))
        raise SystemExit(1)

    def sigterm_handler2(signo, frame):
        #handler = RPCHandler()
        TRACE("sigterm_handler2(signo=%s, frame=%s).  "
              "kill the mplayer process.  pid???"
              % (str(signo), str(frame)))
        #TRACE("mplayer pid = %d)" % (handler.mplif.mplproc.pid))
        #raise SystemExit(1)

    signal.signal(signal.SIGTERM, sigterm_handler)
    signal.signal(signal.SIGUSR1, sigterm_handler2)

    if not os.path.exists(pidfile):
        TRACE('WARNING: PID file %s was not created' % (pidfile))

    else:
        # make sure the directory is writable to the user contexts that this is
        # expected to be called from
        from os import chmod, chown
        try:
          chmod(d, 0o771)
          chmod(pidfile, 0o664)
          TRACE('set permissions of "%s" to 0o770\n' % d)

        except:
          sys.stdout.write('Failed to set permissions of "%s" to 0o770\n' % d)

        gid=os.getgid()
        try:
          from pwd import getpwnam
          gid=getpwnam(pid_dir_group).pw_gid
          chown(d, os.getuid(), gid)
          chown(pidfile, os.getuid(), gid)
          TRACE('set group permissions of %s to %s\n' % (d, pid_dir_group))
        except:
          TRACE('Failed to set group ownership of "%s" to "%s"\n'
                          % (d, pid_dir_group))

    TRACE("exit daemonize")


def rpc_server(handler, address, authkey):
    sock = Listener(address, authkey=authkey)
    while True:
        try:
            client = sock.accept()
            #except socket.error:
            #    TRACE("socket.error exception: connection broken")
            #except:
            #    TRACE("exception in sock.accept():")
            #    #traceback.print_exc()
            t = Thread(target=handler.handle_connection, args=(client,))
            t.daemon = True
            t.start()
            # Some remote functions
        except:
            TRACE("exception in sock.accept().  "
                  "breaking out of rpc_server accept loop")
            #traceback.print_exc()
            break

def daemon_start():
    pidfile = config.PIDFILE
    try:
        daemonize(
            pidfile,
            stdout=config.stdout_file,
            stderr=config.stderr_file
        )
        sys.stdout.write('daemonize %s returned (pidfile %s)\n'
                         % (sys.argv[0], pidfile))
        #TRACE("started %s iwth
    except RuntimeError as e:
        sys.stdout.write("Exception: %s\n" % str(e))
        #raise SystemExit(1)
        sys.exit(1)
    main()

def kill_process(pidfile, sig):
    if os.path.exists(pidfile):
        with open(pidfile) as f:
            pid = -1
            try:
                pid = int(f.read())
                if check_pid(pid):
                    try:
                        TRACE("sending a kill(%s) to process %s (PID %d)..."
                              % (str(sig), str(DAEMON), pid))
                        os.kill(pid, sig)
                    except:
                        TRACE("exception sending a %s signal to process %s "
                              "(PID %d)..."
                              % (str(sig), DAEMON, pid))
                        e = sys.exc_info()
                        TRACE("exception info: %s" % str(e))
                        traceback.print_exc()
                    # FIXME: don't exit until the process is confirmed dead
                else:
                    TRACE('process %d has been terminated.' % (pid))
            except:
                TRACE('exception reading the process id')
                e = sys.exc_info()
                TRACE("exception info: %s" % str(e))
                traceback.print_exc()

        if os.path.exists(pidfile):
            TRACE('WARNING: PID file %s still exists.' % (pidfile))


        """
        if os.path.exists(pidfile):
            TRACE('killed process %d, but the PID file %s still exists.  '
                  'removing...'
                  % (pid, pidfile))
            try:
                os.remove(pidfile)
            except OSError:
                pass
            except:
                TRACE('exception removing pidfile %s, process %d.'
                      % (pidfile, pid))
        except:
            e = sys.exc_info()
            TRACE("exception info: %s" % str(e))
            traceback.print_exc()
        """


        return 0
    else:
        sys.stdout.write('Not running - pidfile %s not found\n' % pidfile)
        # TODO: consider looking for a process with the same name as
        #       os.path.basename(sys.argv[0])
        return 1

def help():
    sys.stdout.write(
        'Usage: %s [start|stop|restart|status|help]'.format(sys.argv[0])
        % sys.argv[0]
    )
    sys.stdout.write(
        "Once the daemon is launched (via '%s start'), "
        "a client can make requests\n"
        "The current client is '%s'\n"
        "Commands are executed as 'pyplayer.py function' or 'pyplayer.py "
        "function arg1 arg2'\n"
        % (os.path.basename(sys.argv[0]), sys.argv[0])
    )
    enumerate_functions()

def daemon_help():
    help()

def daemon_stop():
    failed_to_kill = False
    kill_process(config.PIDFILE, signal.SIGUSR1)
    if (not kill_process(config.MPLAYER_PIDFILE, signal.SIGTERM)):
        failed_to_kill = True
    time.sleep(2)
    if (not kill_process(config.PIDFILE, signal.SIGTERM)):
        failed_to_kill = True
    if os.path.exists(config.MPLAYER_PIDFILE):
        TRACE('WARNING: PID file %s still exists.  killing...'
              % (config.MPLAYER_PIDFILE))
        try:
            os.remove(config.MPLAYER_PIDFILE)
        except OSError:
            pass
        except:
            TRACE('exception removing pidfile %s' % (config.MPLAYER_PIDFILE))
    if failed_to_kill:
            raise SystemExit(1)


def daemon_status():

    d = os.path.dirname(config.PIDFILE)
    if not os.path.exists(d):
        sys.stdout.write('Not running.  '
              'pyplayer run directory %s does not exist.  this '
              'folder must be created, with write and excute permissions of '
              'the current process\n' % d)
        raise SystemExit(1)

    if not os.path.exists(config.PIDFILE):
        sys.stdout.write('Not running.  PIDFILE %s does not exist.\n' % config.PIDFILE)
        raise SystemExit(1)

    pid = int(open(config.PIDFILE).read())
    # example: cmdlinefile="/projects/6124/cmdline"
    cmdlinefile = "/proc/%d/cmdline" % pid
    if not os.path.exists(cmdlinefile):
        sys.stdout.write(
            'Not running.  cmdline file %s does not exist in /proc tree.\n'
            % cmdlinefile
        )
        raise SystemExit(1)

    s = open(cmdlinefile).readline()
    # example: s='python3\x00./pyplayerd.py\x00start\x00'
    s2 = s.split("\0")[1]
    s3 = os.path.basename(s2)
    daemon = os.path.basename(DAEMON)
    # example: s2='./pyplayerd.py'
    if not s3 == daemon:
        sys.stdout.write(
            'Not running.  cmdline file %s indicates %s; expected %s\n'
            % (cmdlinefile, s3, daemon)
        )
        raise SystemExit(1)

    sys.stdout.write('%s is running (pid=%d)\n' % (DAEMON, pid))

def daemon_restart():
    try:
        daemon_stop()
    except:
        pass
    daemon_start()

def main():
    TRACE('Daemon started with pid {}'.format(os.getpid()))
    # Register with a handler
    handler = RPCHandler()
    for f in getmembers(handler.mplif):
        if f[1] is None: continue
        if ismethod(f[1]) and f[0][0] != '_':
            handler.register_function(f[1])
    # Run the server
    rpc_server(handler, ('localhost', 17000), authkey=b'super_secret_auth_key')


if __name__ == '__main__':

    #logging.basicConfig(format='%(asctime)s pyplayerd.py %(message)s')
    #logger = logging.getLogger()
    #logger.setLevel(logging.DEBUG)
    #logging.basicConfig(format='%(asctime) %(message)s')
    #logging.basicConfig(format='%%(asctime)s %s %%(message)s' % ME)
    #logging.log(logging.WARNING, 'Initialized the logging interface for %s'
    #                              % sys.argv[0])
    #logging.warning('Initialized the %s logging interface.' % sys.argv[0])



    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    #ch.setLevel(logging.DEBUG)
    # create formatter
    formatter = logging.Formatter(
        "%(asctime)s: pyplayerd: %(name)s: %(levelname)s: %(message)s")
    # add formatter to ch
    ch.setFormatter(formatter)



    if logger.handlers:
        sys.stderr.write("pyplayerd.py (%s) already has log handlers: %s\n"
            % (sys.argv[0], str(logger.handlers)))
    else:
        # add ch to logger
        logger.addHandler(ch)


    # "application" code
    '''
    TRACE("debug message", logging.DEBUG)
    TRACE("info message", logging.INFO)
    TRACE("warn message", logging.WARNING)
    TRACE("error message", logging.ERROR)
    TRACE("critical message", logging.CRITICAL)
    '''



    PIDFILE = config.PIDFILE
    TRACE("PIDFILE = %s" % PIDFILE)
    DAEMON = sys.argv[0]
    if len(sys.argv) != 2:
        print(
            'Usage: {} [start|stop|restart|status|help]'.format(sys.argv[0])
        )
        raise SystemExit(1)
    if sys.argv[1] == 'start':
        daemon_start()
    elif sys.argv[1] == 'stop':
        daemon_stop()
    elif sys.argv[1] == 'status':
        daemon_status()
    elif sys.argv[1] == 'restart':
        daemon_restart()
    elif sys.argv[1] == 'functions':
        enumerate_functions()
    elif sys.argv[1] == 'help':
        help()
    else:
        TRACE('Unknown command {!r}'.format(sys.argv[1]))
        raise SystemExit(1)

