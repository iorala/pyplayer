# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
@brief various settings

@author: Bryant Hansen <sourcecode@bryanthansen.net>
@license: GPLv3
"""

# TODO: this should be written as a conf file if it's to be edited by users
#       XML is the alternative, but it's a bit complicated for normal users

from os import sep, getuid, getenv

servername = 'localhost'
SERVER = servername
port = 17000
PORT = port
authkey = b'super_secret_auth_key'

fifo_dir = "/tmp"
fifo_dir = "/projects/critter_list/fifo"

url = ""
ififo = fifo_dir + "/mplayer.stdin.fifo"
#log = "/projects/critter_list/log/mplayer.log"

pidfile = '/run/pyplayer/pyplayerd.pid'

#LOGDIR = '/projects/critter_list/log'
LOGDIR = "/var/log/pyplayer"
LOGFILE = ("%s%s%s" % (LOGDIR, sep, 'pyplayer.log'))
stdout_file = ("%s%s%s" % (LOGDIR, sep, "pyplayer.out"))
stderr_file = ("%s%s%s" % (LOGDIR, sep, "pyplayer.err"))

if getuid() == 0:
    PIDFILE = '/run/pyplayer/pyplayerd.pid'
    MPLAYER_PIDFILE = '/run/pyplayer/mplayer.pid'
else:
    PIDFILE = '%s/.run/pyplayer/pyplayerd.pid' % getenv("HOME")
    MPLAYER_PIDFILE = '%s/.run/pyplayer/mplayer.pid' % getenv("HOME")

gongsound = "/projects/critter_list/pyplayer/sounds/gong9.wav"
