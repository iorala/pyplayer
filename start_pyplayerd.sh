#!/bin/sh
# Bryant Hansen

# Intended to be run as root
# DO NOT SYMLINK TO THIS; install manually to avoid security issues - privilege elevation from git committer to root access

ME="$(basename "$0")"

trace() {
    local LINE_PREFIX="`date "+%Y%m%d %H%M%S"`: "
    echo -e "${LINE_PREFIX}:${ME}:$*" >&2
}

# defaults - at the moment, the same as in the conf
RUN_FILE=/run/pyplayer/pyplayerd.pid
PYPLAYERD_USER=pyplayerd
PYPLAYERD_GROUP=pyplayerd
PYPLAYERD=/usr/local/bin/pyplayerd

CONF_FILE="/etc/pyplayer/pyplayerd.conf"
[[ -f "$CONF_FILE" ]] && . "$CONF_FILE"
CONF_FILE="~/pyplayer/pyplayerd.conf"
[[ -f "$CONF_FILE" ]] && . "$CONF_FILE"
CONF_FILE="./pyplayerd.conf"
[[ -f "$CONF_FILE" ]] && . "$CONF_FILE"

if [[ -f "$PYPLAYERD" ]] && [[ ! -x "$PYPLAYERD" ]] ; then
    trace "ERROR: $PYPLAYERD is not executable.  Exiting abnormally."
    exit 2
elif [[ ! -f "$PYPLAYERD" ]] && ! which "$PYPLAYERD" > /dev/null ; then    trace "ERROR: $PYPLAYERD not found at the specified location nor in the path.  Exiting abnormally."
    exit 3
fi

# check if the directory exists
if [[ ! -d "$(dirname "$RUN_FILE")" ]] && ! mkdir -p "$(dirname "$RUN_FILE")" ; then
    trace "ERROR: could not create the RUN_DIR '$(dirname "$RUN_FILE")'.  Exiting abnormally."
    exit 4
fi

# check if the directory exists after our creation attempt
if [[ ! -d "$(dirname "$RUN_FILE")" ]] ; then
    trace "ERROR: failed to create the RUN_DIR '$(dirname "$RUN_FILE")' without error from mkdir.  Exiting abnormally."
    exit 5
fi

# check if we have permission to set the permissions of the pyplayerd folder
if ! chown "$PYPLAYERD_USER":"$PYPLAYERD_GROUP" "$(dirname "$RUN_FILE")" ; then
    trace "ERROR: could not set the permissions of the RUN_DIR '$(dirname "$RUN_FILE")' to '${PYPLAYERD_USER}:${PYPLAYERD_GROUP}'.  Exiting abnormally."
    exit 6
fi

if ! su "$PYPLAYERD_USER" -c "$PYPLAYERD" start ; then
    trace "ERROR: '$PYPLAYERD' start returns failure"
    exit 7
fi

if ! pgrep "$PYPLAYERD" ; then
    trace "WARNING: no failure occurred, but there is no indication that '$PYPLAYERD' has started."
fi

exit 0
